use std::io::{self, Write};
#[macro_use]
mod roff;
mod man;

use roff::{Troffable, TroffWriter};
use man::{ManWriter};

fn build_man() -> io::Result<Vec<u8>> {
    let mut buf : Vec<u8> = vec![];
    {
        let mw = man::Writer::new(&mut buf, "prog", 1);
        mw.section("name")?
            .para()?
            .text("prog - does stuff\n")? // XXX: keep nl state?
            .section("options")?
            // This is what the description for grep's '-f' option would look like
            .para_heading(man::Acc::new()
                          .m_bold_it(troff_vec!["-f",
                                                " FILE",
                                                man::Acc::new()
                                                .regular(",")?
                                                .text("--file=")?,
                                                " FILE"
                          ])?)?
            .text(man::Acc::new()
                  .text("Obtain patterns from\n")?
                  .m_it_regular(vec!["FILE", ","])?
                  .text("one per line.\n")?
                  .text("If this option is used multiple times or is combined with the\n")?
                  .m_bold(vec!["-e"])?
                  .m_regular_bold(vec!["(", "--regexp", ")"])?
                  .text("option, search for all patterns given.\n")?
                  .text("The empty file contains zero patterns, and therefore matches nothing.\n")?
            )?;
    }
    Ok (buf)
}

fn main() {
    io::stdout().write_all(&(build_man().unwrap())).unwrap();
}
