use std::io::{self, Write};
use std::marker::PhantomData;

// Occasionally, the user needs to build a slice of Troffable items,
// not all of which have the same type. Provide a utility macro
// which calls render() on each item (so that they all have type Troff)
// and construct a vector of the results.
#[macro_export]
macro_rules! troff_vec {
    ( $( $e:expr ),* ) => {
        vec![$( ($e).render(), )* ]
    }
}

macro_rules! fontf {
    ( $func:ident, $m:expr ) => {
        fn $func <T> (self, t: T) -> io::Result<Self>
        where
            T: Troffable<MacroPkg>,
            Self: Sized,
        {
            self.with_font($m, t)
        }
    }
}

/// Contains valid troff bytes. I.e properly escaped and with valid troff
/// for switching fonts, etc. The type can contain base troff macros and
/// an additional set of macros provided by the `MacroPkg`. The idea is
/// that the user should get an error if they try to mix e.g. man and
/// mdoc macros in the same document.
pub struct Troff<MacroPkg>(String, PhantomData<MacroPkg>);

impl<MacroPkg> Troff<MacroPkg> {
    // Allows us to finally write our troff output
    fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }
}

/// Types which can be formatted as roff + `MacroPkg`
pub trait Troffable<MacroPkg> {
    fn render(self) -> Troff<MacroPkg>;
}

impl<'a, MacroPkg> Troffable<MacroPkg> for &'a str
{
    fn render(self) -> Troff<MacroPkg> {
        Troff(self.replace("-", r"\-"), PhantomData)
    }
}

impl<MacroPkg> Troffable<MacroPkg> for Troff<MacroPkg> {
    fn render(self) -> Troff<MacroPkg> {
        self
    }
}

/// Wraps an `io::Write` object and serializes roff macros to it
pub struct Writer<'a, MacroPkg> {
    out: &'a mut io::Write,
    _phantom: PhantomData<MacroPkg>,
}

impl<'a, MacroPkg> Writer<'a, MacroPkg> {
    pub fn new(w : &mut io::Write) -> Writer<MacroPkg> {
        Writer {
            out: w,
            _phantom: PhantomData,
        }
    }
}

/// Wraps an internal buffer and serializes roff macros to it
///
/// This is separate to `Writer` only because it makes it easier
/// to construct a roff document on the fly, for use with functions
/// that expect a `Troffable`.
pub struct Acc<MacroPkg> {
    acc: Vec<u8>,
    _phantom: PhantomData<MacroPkg>,
}

impl<MacroPkg> Acc<MacroPkg> {
    pub fn new() -> Acc<MacroPkg> {
        Acc {
            acc: vec![],
            _phantom: PhantomData,
        }
    }
}

impl<MacroPkg> Troffable<MacroPkg> for Acc<MacroPkg> {
    fn render(self) -> Troff<MacroPkg> {
        Troff(String::from_utf8(self.acc).unwrap(), PhantomData)
    }
}

/// trait for objects that allow writting roff-formatted output
///
/// This trait takes care of the escaping and provides some utility functions
/// to ensure consistent behavior.
pub trait TroffWriter<MacroPkg> {
    fn writer(&mut self) -> &mut Write;
    fn text<T>(mut self, t: T) -> io::Result<Self>
    where
        T: Troffable<MacroPkg>,
        Self: Sized,
    {
        {
            let w = self.writer();
            w.write_all(t.render().as_bytes())?;
        }
        Ok (self)
    }
    fn with_font<T>(mut self, ff: &str, t : T) -> io::Result<Self>
    where
        T: Troffable<MacroPkg>,
        Self: Sized,
    {
        {
            let w = self.writer();
            write!(w, r"\f{}", ff)?;
            w.write_all(t.render().as_bytes())?;
            write!(w, r"\fP")?;
        }
        Ok (self)
    }
    fontf!(regular, "R");
    fontf!(bold, "B");
    fn mac<S, T>(mut self, s: S, args : Vec<T>) -> io::Result<Self>
    where S: AsRef<str>,
          T: Troffable<MacroPkg>,
          Self: Sized,
    {
        {
            let w = self.writer();
            // XXX: Need to ensure `s` points to a valid macro name
            write!(w, ".{}", s.as_ref())?;
            for a in args {
                write!(w, " \"")?;
                w.write_all(a.render().as_bytes())?;
                write!(w, "\"")?;
            }
            write!(w, "\n")?;
        }
        Ok (self)
    }
}

impl<'a, MacroPkg> TroffWriter<MacroPkg> for Writer<'a, MacroPkg> {
    fn writer(&mut self) -> &mut Write {
        self.out
    }
}

impl<MacroPkg> TroffWriter<MacroPkg> for Acc<MacroPkg> {
    fn writer(&mut self) -> &mut Write {
        &mut self.acc
    }
}
