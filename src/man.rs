use std::io::{self, Write};
use super::roff::{self, Troffable, TroffWriter, Troff};

macro_rules! fontf {
    ( $func:ident, $m:expr ) => {
        fn $func <T>(self, t: Vec<T>) -> io::Result<Self>
        where
            T: Troffable<Man>,
            Self: Sized,
        {
            self.mac($m, t)
        }
    }
}

/// Used as the `MacroPkg` type expected by the backing roff types
pub struct Man;

/// Wraps an internal `roff::Acc`
pub struct Acc {
    acc: roff::Acc<Man>,
}


impl Acc {
    pub fn new() -> Acc {
        Acc {
            acc: roff::Acc::new(),
        }
    }
}

impl Troffable<Man> for Acc {
    fn render(self) -> Troff<Man> {
        self.acc.render()
    }
}

/// trait for objects that allow writing man-formatted output
pub trait ManWriter: TroffWriter<Man> {
    fn section(self, t: &str) -> io::Result<Self>
    where
        Self: Sized,
    {
        self.mac("SH", vec![t])
    }
    fn para(self) -> io::Result<Self>
    where
        Self: Sized,
    {
        let nada : Vec<&str> = vec![];
        self.mac("PP", nada)
    }
    fn para_heading<T>(self, h: T) -> io::Result<Self>
    where
        T : Troffable<Man>,
        Self: Sized,
    {
        let nada : Vec<&str> = vec![];
        self.mac("TP", nada)?
            .text(h)
    }
    fontf!(m_bold, "B");
    fontf!(m_bold_it, "BI");
    fontf!(m_it_regular, "IR");
    fontf!(m_regular_bold, "RB");
}

/// Wraps a `roff::Writer`
pub struct Writer<'a> {
    tw: roff::Writer<'a, Man>,
}

impl<'w> Writer<'w> {
    pub fn new(w: &'w mut io::Write, title : &str, section: usize) -> Writer<'w>
    {
        let tw = roff::Writer::new(w);
        let sec = format!("{}", section);
        let tw : roff::Writer<Man> = tw.mac("TH", vec![title, &sec]).unwrap();
        Writer {
            tw
        }
    }
}

impl<'a> TroffWriter<Man> for Writer<'a> {
    fn writer(&mut self) -> &mut Write {
        self.tw.writer()
    }
}

impl TroffWriter<Man> for Acc {
    fn writer(&mut self) -> &mut Write {
        self.acc.writer()
    }
}

impl<'a> ManWriter for Writer<'a> {}
impl ManWriter for Acc {}
